#!/bin/sh

#build and install freetype
cd $IMAGINE_PATH/bundle/all/src/freetype
make -f android-armv7.mk install

#build and install libogg
cd $IMAGINE_PATH/bundle/all/src/libogg
make -f android-armv7.mk install

#build and install libpng
cd $IMAGINE_PATH/bundle/all/src/libpng
make -f android-armv7.mk install

#build and install libsndfile
cd $IMAGINE_PATH/bundle/all/src/libsndfile
make -f android-armv7.mk install

#build and install minizip
cd $IMAGINE_PATH/bundle/all/src/minizip
make -f android-armv7.mk install

#build and install tremor
cd $IMAGINE_PATH/bundle/all/src/tremor
make -f android-armv7.mk install

#install boost
cd $IMAGINE_PATH/bundle/all/src/boost
make -f android-armv7.mk
make -f android-armv7.mk install

#copy *.pc to pkgconfig
cp $IMAGINE_PATH/bundle/android/armv7/lib/pkgconfig/*.pc $IMAGINE_PATH/pkgconfig
