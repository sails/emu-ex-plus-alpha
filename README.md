emu-ex-plus-alpha
=================

Multi-platform computer &amp; game console emulation system including supporting code (EmuFramework) and core engine (Imagine)


# My build environment as follows(ubuntu 14.04): #

**android toolchain setting**

    $NDK/build/tools/make-standalone-toolchain.sh --platform=android-9 --toolchina=arm-linux-androideabi-4.8 --install-dir=/home/sails/software/my-android-toolchain

<http://www.kandroid.org/ndk/docs/STANDALONE-TOOLCHAIN.html>

**environment setting**

    export JAVA_HOME="/var/lib/jvm/jdk1.8.0"
    export CLASSPATH=".:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar"
    export NDK="/usr/local/service/android-ndk-r9d"
    export ANDROID_NDK_PATH="$NDK"
    export ANDROID_SDK_PATH="/home/sails/software/adt-bundle-linux/sdk"
    export ANT_HOME="/usr/local/service/apache-ant"
    export PATH="$PATH:$JAVA_HOME/bin:$ANDROID_SDK_PATH/platforms:$ANDROID_SDK_PATH/platform-tools:$ANDROID_SDK_PATH/tools:/home/sails/software/my-android-toolchain/bin:$ANT_HOME/bin"
    
    export IMAGINE_PATH=$(pwd)/imagine
    export CHOST=arm-linux-androideabi
    export host=arm-linux-androideabi
    export SYSROOT="/home/sails/software/my-android-toolchain/sysroot"
    export CC="arm-linux-androideabi-gcc"


**build**

Project default compiler for the release version, and while compiling arm, x86 and armv7 three different architectures, so you may need to set:

    android_antTarget:=debug
    android_noArch := arm x86

such as NES.emu/android-9.mk:

    metadata_confDeps := ../EmuFramework/metadata/conf.mk
    android_antTarget:=debug
    android_noArch := arm x86
    include $(IMAGINE_PATH)/make/shortcut/meta-builds/android-9.mk



ubuntu use dash for default in interactive shell and "echo -e" will be an error, so you can

    sudo dpkg-reconfigure dash

to change to bash

**other build tools:**

    make :  3.8.2
	automake: 1.14
    autoconf
    libtool

If you're compiling on a 64-bit system, you may encounter an error, because some android executable file runs only on 32-bit systems,
so you need to install the ia32-libs:

    sudo -i
    cd /etc/apt/sources.list.d
    echo "deb http://archive.ubuntu.com/ubuntu/ raring main restricted universe multiverse" >ia32-libs-raring.list
    apt-get update
    apt-get install ia32-libs

