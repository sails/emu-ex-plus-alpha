#!/bin/sh

#build imagine
cd $IMAGINE_PATH
make -f android-9-armv7-release.mk

#copy *.pc to pkgconfig
cp $IMAGINE_PATH/lib/android-9-armv7-release/imagine.pc $IMAGINE_PATH/pkgconfig
